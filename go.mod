module chainmaker.org/chainmaker/vm-native/v2

go 1.16

require (
	chainmaker.org/chainmaker/chainconf/v2 v2.3.4
	chainmaker.org/chainmaker/common/v2 v2.3.5
	chainmaker.org/chainmaker/localconf/v2 v2.3.5
	chainmaker.org/chainmaker/logger/v2 v2.3.4
	chainmaker.org/chainmaker/pb-go/v2 v2.3.6
	chainmaker.org/chainmaker/protocol/v2 v2.3.6
	chainmaker.org/chainmaker/utils/v2 v2.3.5
	github.com/gogo/protobuf v1.3.2
	github.com/golang/mock v1.6.0
	github.com/golang/protobuf v1.5.2
	github.com/google/uuid v1.1.2
	github.com/mr-tron/base58 v1.2.0
	github.com/pingcap/goleveldb v0.0.0-20191226122134-f82aafb29989
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.7.0
)
