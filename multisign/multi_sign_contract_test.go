/*
 * Copyright (C) BABEC. All rights reserved.
 * Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 */
package multisign

import (
	"fmt"
	"testing"

	"chainmaker.org/chainmaker/utils/v2"

	"chainmaker.org/chainmaker/logger/v2"

	"chainmaker.org/chainmaker/common/v2/crypto"
	"chainmaker.org/chainmaker/pb-go/v2/accesscontrol"
	pbac "chainmaker.org/chainmaker/pb-go/v2/accesscontrol"
	commonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	configPb "chainmaker.org/chainmaker/pb-go/v2/config"
	storePb "chainmaker.org/chainmaker/pb-go/v2/store"
	"chainmaker.org/chainmaker/pb-go/v2/syscontract"
	"chainmaker.org/chainmaker/protocol/v2"
	"chainmaker.org/chainmaker/protocol/v2/mock"
	"chainmaker.org/chainmaker/protocol/v2/test"
	"chainmaker.org/chainmaker/vm-native/v2/common"
	"github.com/gogo/protobuf/proto"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

var (
	chainId          = "chain1"
	height           = uint64(0)
	txId             = "56789"
	log              = &test.GoLogger{}
	blockVersion2330 = uint32(2030300)

	cname   = syscontract.SystemContract_CONTRACT_MANAGE.String()
	cmethod = syscontract.ContractManageFunction_INIT_CONTRACT.String()
)

func newContractInitPairs() []*commonPb.KeyValuePair {
	return []*commonPb.KeyValuePair{
		{
			Key:   syscontract.MultiReq_SYS_CONTRACT_NAME.String(),
			Value: []byte(syscontract.SystemContract_CONTRACT_MANAGE.String()),
		},
		{
			Key:   syscontract.MultiReq_SYS_METHOD.String(),
			Value: []byte(syscontract.ContractManageFunction_INIT_CONTRACT.String()),
		},
		{
			Key:   syscontract.InitContract_CONTRACT_NAME.String(),
			Value: []byte("testContractName"),
		},
		{
			Key:   syscontract.InitContract_CONTRACT_VERSION.String(),
			Value: []byte("v1"),
		},
		{
			Key:   syscontract.InitContract_CONTRACT_BYTECODE.String(),
			Value: []byte("byte code!!!"),
		},
		{
			Key:   syscontract.InitContract_CONTRACT_RUNTIME_TYPE.String(),
			Value: []byte("WASMER"),
		},
	}
}

func initParameters() map[string][]byte {
	parameters := make(map[string][]byte)
	parameters[syscontract.MultiReq_SYS_CONTRACT_NAME.String()] = []byte(cname)
	parameters[syscontract.MultiReq_SYS_METHOD.String()] = []byte(cmethod)
	parameters[syscontract.InitContract_CONTRACT_NAME.String()] = []byte("testContractName")
	parameters[syscontract.InitContract_CONTRACT_VERSION.String()] = []byte("v1")
	parameters[syscontract.InitContract_CONTRACT_BYTECODE.String()] = []byte("byte code!!!")
	parameters[syscontract.InitContract_CONTRACT_RUNTIME_TYPE.String()] = []byte("WASMER")
	return parameters
}

func initVoteParameters() map[string][]byte {
	parameters := make(map[string][]byte)
	msvi := &syscontract.MultiSignVoteInfo{
		Vote: syscontract.VoteStatus_AGREE,
		Endorsement: &commonPb.EndorsementEntry{
			Signer: getOrg1Client1Signer(),
		},
	}
	msviByte, _ := msvi.Marshal()
	parameters[syscontract.MultiVote_VOTE_INFO.String()] = msviByte
	parameters[syscontract.MultiVote_TX_ID.String()] = []byte("05a0e329c4c94e909575214b41ca516716bd8c83dea94c4c9616dea0910bf719")
	return parameters
}

func TestNewMultiSignContract(t *testing.T) {
	contracts := make(map[string]common.Contract, 64)
	contracts[syscontract.SystemContract_MULTI_SIGN.String()] = NewMultiSignContract(log)
}

func TestInitMultiContractMethods(t *testing.T) {
	commonFunc := InitMultiContractMethods(log)
	fmt.Println(commonFunc)
}

func TestReq(t *testing.T) {

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	InitMultiContractMethods(log)
	r := NewMultiSignRuntime()

	chainConfig := &configPb.ChainConfig{
		Vm: &configPb.Vm{
			Native: &configPb.VmNative{
				Multisign: &configPb.MultiSign{
					EnableManualRun: true,
				},
			},
		},
	}
	blockchainStore := mock.NewMockBlockchainStore(ctrl)
	blockchainStore.EXPECT().GetLastChainConfig().Return(chainConfig, nil).AnyTimes()
	txSimContext := mock.NewMockTxSimContext(ctrl)
	txSimContext.EXPECT().GetBlockVersion().Return(blockVersion2330).AnyTimes()
	txSimContext.EXPECT().GetLastChainConfig().Return(chainConfig).AnyTimes()
	txSimContext.EXPECT().GetBlockchainStore().Return(blockchainStore).AnyTimes()

	tx := &commonPb.Transaction{
		Payload: &commonPb.Payload{
			ContractName: syscontract.SystemContract_MULTI_SIGN.String(),
			Method:       syscontract.MultiSignFunction_REQ.String(),
			Parameters:   newContractInitPairs(),
		},
		Sender: &commonPb.EndorsementEntry{
			Signer: getOrg1Client1Signer(),
		},
	}
	txBytes, err := utils.CalcUnsignedTxBytes(tx)
	assert.Nil(t, err)
	txSimContext.EXPECT().GetTx().Return(tx).AnyTimes()
	txSimContext.EXPECT().Put(gomock.Any(), gomock.Any(), gomock.Any()).AnyTimes()
	txSimContext.EXPECT().GetSender().Return(&pbac.Member{MemberInfo: []byte("user1")}).AnyTimes()

	acTest := mock.NewMockAccessControlProvider(ctrl)
	acTest.EXPECT().IsRuleSupportedByMultiSign(gomock.Any(), gomock.Any()).Return(nil).AnyTimes()
	acTest.EXPECT().VerifyMultiSignTxPrincipal(
		gomock.Any(), gomock.Any()).Return(syscontract.MultiSignStatus_PROCESSING, nil).AnyTimes()
	principal := mock.NewMockPrincipal(ctrl)
	principal.EXPECT().GetResourceName().Return(
		syscontract.SystemContract_CONTRACT_MANAGE.String() + "-" +
			syscontract.ContractManageFunction_INIT_CONTRACT.String()).AnyTimes()
	principal.EXPECT().GetMessage().Return(txBytes).AnyTimes()
	principal.EXPECT().GetEndorsement().Return(tx.Endorsers).AnyTimes()
	acTest.EXPECT().CreatePrincipal(gomock.Any(), gomock.Any(), gomock.Any()).Return(principal, nil).AnyTimes()
	acTest.EXPECT().GetValidEndorsements(gomock.Any(), gomock.Any()).Return(tx.Endorsers, nil).AnyTimes()
	txSimContext.EXPECT().GetAccessControl().Return(acTest, nil).AnyTimes()

	// 准备构建测试对象
	runtime := &MultiSignRuntime{log: &test.GoLogger{}}

	// 【valid】
	result := runtime.Req(txSimContext, initParameters())
	assert.Equal(t, uint32(0), result.Code)
	t.Log(result)
	result = runtime.Req(txSimContext, initParameters())
	assert.Equal(t, uint32(0), result.Code)
	t.Log(result)

	parameters := map[string][]byte{}
	parameters[syscontract.MultiReq_SYS_CONTRACT_NAME.String()] = []byte("123")
	parameters[syscontract.MultiReq_SYS_METHOD.String()] = []byte("123")

	result = r.Req(txSimContext, parameters)
	assert.Equal(t, uint32(1), result.Code)
	t.Log(result)
}

func TestVote(t *testing.T) {

	InitMultiContractMethods(log)

	ctrl := gomock.NewController(t)
	chainConfig := &configPb.ChainConfig{
		Vm: &configPb.Vm{
			Native: &configPb.VmNative{
				Multisign: &configPb.MultiSign{
					EnableManualRun: true,
				},
			},
		},
	}
	blockchainStore := mock.NewMockBlockchainStore(ctrl)
	blockchainStore.EXPECT().GetLastChainConfig().Return(chainConfig, nil).AnyTimes()
	txSimContext := mock.NewMockTxSimContext(ctrl)
	txSimContext.EXPECT().GetBlockVersion().Return(blockVersion2330).AnyTimes()
	txSimContext.EXPECT().GetContractByName(syscontract.SystemContract_MULTI_SIGN.String()).Return(&commonPb.Contract{Name: syscontract.SystemContract_MULTI_SIGN.String()}, nil).AnyTimes()

	defer ctrl.Finish()
	//txSimContext.EXPECT().GetTx().Return(&commonPb.Payload{ChainId: "chain1",TxType: 0,TxId: "05a0e329c4c94e909575214b41ca516716bd8c83dea94c4c9616dea0910bf719",Timestamp: 0,ExpirationTime: 300,ContractName: "CONTRACT_MANAGE",Method: "INIT_CONTRACT",Parameters: newContractInitPairs()}).AnyTimes()
	txSimContext.EXPECT().GetTx().DoAndReturn(
		func() *commonPb.Transaction {
			return &commonPb.Transaction{
				Payload: &commonPb.Payload{
					TxId:         "05a0e329c4c94e909575214b41ca516716bd8c83dea94c4c9616dea0910bf719",
					ContractName: "CONTRACT_MANAGE",
					Method:       "INIT_CONTRACT",
					Parameters:   newContractInitPairs(),
				},
				Sender: &commonPb.EndorsementEntry{
					Signer: getOrg1Client1Signer(),
				},
			}
		}).AnyTimes()
	txSimContext.EXPECT().GetBlockchainStore().Return(blockchainStore).AnyTimes()
	multiSignInfo := &syscontract.MultiSignInfo{
		Payload: &commonPb.Payload{
			TxId:         "05a0e329c4c94e909575214b41ca516716bd8c83dea94c4c9616dea0910bf719",
			ContractName: "CONTRACT_MANAGE",
			Method:       "INIT_CONTRACT",
			Parameters:   newContractInitPairs(),
		},
		ContractName: syscontract.SystemContract_CONTRACT_MANAGE.String(),
		Method:       syscontract.ContractManageFunction_INIT_CONTRACT.String(),
		Status:       syscontract.MultiSignStatus_PROCESSING,
		VoteInfos:    nil,
	}
	multiSignInfoBytes, _ := multiSignInfo.Marshal()

	multiSignVoteInfo := &syscontract.MultiSignVoteInfo{
		Vote: syscontract.VoteStatus_AGREE,
		Endorsement: &commonPb.EndorsementEntry{
			Signer:    getOrg1Client1Signer(),
			Signature: []byte("Signature_data"),
		},
	}
	multiSignVoteBytes, err := multiSignVoteInfo.Marshal()
	assert.Nil(t, err)

	txSimContext.EXPECT().GetBlockVersion().Return(blockVersion2330).AnyTimes()
	txSimContext.EXPECT().Get(gomock.Any(), gomock.Any()).Return(multiSignInfoBytes, nil).AnyTimes()
	txSimContext.EXPECT().GetSender().Return(&pbac.Member{MemberInfo: []byte("user1")}).AnyTimes()
	txSimContext.EXPECT().Put(gomock.Any(), gomock.Any(), gomock.Any()).AnyTimes()
	acTest := mock.NewMockAccessControlProvider(ctrl)
	acTest.EXPECT().GetHashAlg().Return("SHA256").AnyTimes()
	acTest.EXPECT().NewMember(gomock.Any()).Return(&Mb{}, nil).AnyTimes()
	acTest.EXPECT().CreatePrincipal(gomock.Any(), gomock.Any(), gomock.Any()).Return(nil, nil).AnyTimes()
	acTest.EXPECT().VerifyMultiSignTxPrincipal(
		gomock.Any(), gomock.Any()).Return(syscontract.MultiSignStatus_PROCESSING, nil).AnyTimes()
	en := commonPb.EndorsementEntry{
		Signer: getOrg1Client1Signer(),
	}
	endorsers := make([]*commonPb.EndorsementEntry, 0)
	endorsers = append(endorsers, &en)
	acTest.EXPECT().GetValidEndorsements(gomock.Any(), gomock.Any()).Return(endorsers, nil).AnyTimes()
	txSimContext.EXPECT().GetAccessControl().Return(acTest, nil).AnyTimes()
	runtime := &MultiSignRuntime{log: logger.GetLogger("test")}

	parameters := make(map[string][]byte, 2)
	parameters[syscontract.MultiVote_TX_ID.String()] = []byte("111")
	parameters[syscontract.MultiVote_VOTE_INFO.String()] = multiSignVoteBytes
	result := runtime.Vote(txSimContext, parameters)
	assert.Equal(t, uint32(0), result.Code)
	t.Log(result)

}

func TestVote2(t *testing.T) {

	InitMultiContractMethods(log)

	ctrl := gomock.NewController(t)
	chainConfig := &configPb.ChainConfig{
		Vm: &configPb.Vm{
			Native: &configPb.VmNative{
				Multisign: &configPb.MultiSign{
					EnableManualRun: true,
				},
			},
		},
	}
	blockchainStore := mock.NewMockBlockchainStore(ctrl)
	blockchainStore.EXPECT().GetLastChainConfig().Return(chainConfig, nil).AnyTimes()
	txSimContext := mock.NewMockTxSimContext(ctrl)
	defer ctrl.Finish()
	//txSimContext.EXPECT().GetTx().Return(&commonPb.Payload{ChainId: "chain1",TxType: 0,TxId: "05a0e329c4c94e909575214b41ca516716bd8c83dea94c4c9616dea0910bf719",Timestamp: 0,ExpirationTime: 300,ContractName: "CONTRACT_MANAGE",Method: "INIT_CONTRACT",Parameters: newContractInitPairs()}).AnyTimes()
	txSimContext.EXPECT().GetTx().DoAndReturn(
		func() *commonPb.Transaction {
			return &commonPb.Transaction{
				Payload: &commonPb.Payload{
					TxId:         "05a0e329c4c94e909575214b41ca516716bd8c83dea94c4c9616dea0910bf719",
					ContractName: "CONTRACT_MANAGE",
					Method:       "INIT_CONTRACT",
					Parameters:   newContractInitPairs(),
				},
				Sender: &commonPb.EndorsementEntry{
					Signer: getOrg1Client1Signer(),
				},
			}
		}).AnyTimes()
	txSimContext.EXPECT().GetBlockchainStore().Return(blockchainStore).AnyTimes()
	multiSignInfo := &syscontract.MultiSignInfo{
		Payload: &commonPb.Payload{
			TxId:         "05a0e329c4c94e909575214b41ca516716bd8c83dea94c4c9616dea0910bf719",
			ContractName: "CONTRACT_MANAGE",
			Method:       "INIT_CONTRACT",
			Parameters:   newContractInitPairs(),
		},
		ContractName: syscontract.SystemContract_CONTRACT_MANAGE.String(),
		Method:       syscontract.ContractManageFunction_INIT_CONTRACT.String(),
		Status:       syscontract.MultiSignStatus_REFUSED,
		VoteInfos:    nil,
	}
	multiSignInfoBytes, _ := multiSignInfo.Marshal()

	txSimContext.EXPECT().GetBlockVersion().Return(blockVersion2330).AnyTimes()
	txSimContext.EXPECT().Get(gomock.Any(), gomock.Any()).Return(multiSignInfoBytes, nil).AnyTimes()
	txSimContext.EXPECT().GetSender().Return(&pbac.Member{MemberInfo: []byte("user1")}).AnyTimes()
	txSimContext.EXPECT().Put(gomock.Any(), gomock.Any(), gomock.Any()).AnyTimes()
	acTest := mock.NewMockAccessControlProvider(ctrl)
	acTest.EXPECT().GetHashAlg().Return("SHA256").AnyTimes()
	acTest.EXPECT().NewMember(gomock.Any()).Return(&Mb{}, nil).AnyTimes()
	acTest.EXPECT().CreatePrincipal(gomock.Any(), gomock.Any(), gomock.Any()).Return(nil, nil).AnyTimes()
	en := commonPb.EndorsementEntry{
		Signer: getOrg1Client1Signer(),
	}
	endorsers := make([]*commonPb.EndorsementEntry, 0)
	endorsers = append(endorsers, &en)
	txSimContext.EXPECT().GetAccessControl().Return(acTest, nil).AnyTimes()
	runtime := &MultiSignRuntime{log: logger.GetLogger("test")}

	result := runtime.Vote(txSimContext, initVoteParameters())
	assert.Equal(t, uint32(1), result.Code)
	t.Log(result)

}

func TestVerifyMemberVote(t *testing.T) {

	InitMultiContractMethods(log)

	r := NewMultiSignRuntime()

	parameters := map[string][]byte{}
	parameters[syscontract.MultiReq_SYS_CONTRACT_NAME.String()] = []byte("123456")
	parameters[syscontract.MultiReq_SYS_METHOD.String()] = []byte("123456")
	parameters[syscontract.MultiVote_TX_ID.String()] = []byte(txId)

	voteInfo, err := proto.Marshal(&syscontract.MultiSignVoteInfo{Vote: syscontract.VoteStatus_AGREE, Endorsement: &commonPb.EndorsementEntry{
		Signer: &accesscontrol.Member{
			OrgId:      "org1",
			MemberInfo: []byte("org1"),
		},
	},
	})
	require.Nil(t, err)

	parameters[syscontract.MultiVote_VOTE_INFO.String()] = voteInfo

	voteInfoBytes := parameters[syscontract.MultiVote_VOTE_INFO.String()]
	txId := parameters[syscontract.MultiVote_TX_ID.String()]

	txSimContext := getTxSimContext(t)
	multiSignInfoBytes, err := txSimContext.Get(contractName, txId)
	require.Nil(t, err)

	multiSignInfo := &syscontract.MultiSignInfo{}
	err = proto.Unmarshal(multiSignInfoBytes, multiSignInfo)

	require.Nil(t, err)
	// 校验：该多签是否已完成投票
	reqVoteInfo := &syscontract.MultiSignVoteInfo{}

	err = proto.Unmarshal(voteInfoBytes, reqVoteInfo)
	require.Nil(t, err)

	var ac protocol.AccessControlProvider
	ac, err = txSimContext.GetAccessControl()
	require.Nil(t, err)

	err = r.hasVoted(ac, reqVoteInfo, multiSignInfo, txId)
	require.Nil(t, err)
}

func TestInvokeContract(t *testing.T) {

	InitMultiContractMethods(log)

	r := NewMultiSignRuntime()

	parameters := map[string][]byte{}
	parameters[syscontract.MultiReq_SYS_CONTRACT_NAME.String()] = []byte("123456")
	parameters[syscontract.MultiReq_SYS_METHOD.String()] = []byte("123456")
	parameters[syscontract.MultiVote_TX_ID.String()] = []byte(txId)

	voteInfo, err := proto.Marshal(&syscontract.MultiSignVoteInfo{Vote: syscontract.VoteStatus_AGREE, Endorsement: &commonPb.EndorsementEntry{
		Signer: &accesscontrol.Member{
			OrgId:      "org1",
			MemberInfo: []byte("org1"),
		},
	},
	})
	require.Nil(t, err)

	parameters[syscontract.MultiVote_VOTE_INFO.String()] = voteInfo

	txId := parameters[syscontract.MultiVote_TX_ID.String()]
	txSimContext := getTxSimContext(t)
	multiSignInfoBytes, err := txSimContext.Get(contractName, txId)
	require.Nil(t, err)

	multiSignInfo := &syscontract.MultiSignInfo{}
	err = proto.Unmarshal(multiSignInfoBytes, multiSignInfo)

	require.Nil(t, err)
	r.invokeContract(txSimContext, multiSignInfo)
}

func TestQuery(t *testing.T) {

	InitMultiContractMethods(log)

	r := NewMultiSignRuntime()

	parameters := map[string][]byte{}
	parameters[syscontract.MultiReq_SYS_CONTRACT_NAME.String()] = []byte("123456")
	parameters[syscontract.MultiReq_SYS_METHOD.String()] = []byte("123456")
	parameters[syscontract.MultiVote_TX_ID.String()] = []byte(txId)

	txSimContext := getTxSimContext(t)

	result, err := r.Query(txSimContext, parameters)
	require.Nil(t, err)
	fmt.Println(string(result))
}

func NewMultiSignRuntime() *MultiSignRuntime {
	return &MultiSignRuntime{
		log: log,
	}
}

func createNewBlock(chainId string, height uint64) *commonPb.Block {

	block := &commonPb.Block{
		Header: &commonPb.BlockHeader{
			BlockHeight:    height,
			PreBlockHash:   nil,
			BlockHash:      nil,
			BlockVersion:   0,
			DagHash:        nil,
			RwSetRoot:      nil,
			BlockTimestamp: 1,
			Proposer:       &pbac.Member{MemberInfo: []byte{1, 2, 3}},
			ConsensusArgs:  nil,
			TxCount:        0,
			Signature:      nil,
			ChainId:        chainId,
		},
		Dag: &commonPb.DAG{
			Vertexes: nil,
		},
	}
	block.Header.PreBlockHash = nil

	return block
}

func getStoreByBlock(t *testing.T, block *commonPb.Block, chainId string, txId string) protocol.BlockchainStore {

	ctrl := gomock.NewController(t)
	store := mock.NewMockBlockchainStore(ctrl)

	store.EXPECT().GetBlock(gomock.Any()).Return(block, nil).AnyTimes()
	store.EXPECT().GetBlockWithRWSets(gomock.Any()).Return(nil, nil).AnyTimes()
	store.EXPECT().GetBlockHeaderByHeight(gomock.Any()).Return(&commonPb.BlockHeader{
		ChainId: chainId,
	}, nil).AnyTimes()

	store.EXPECT().GetLastConfigBlock().Return(block, nil).AnyTimes()
	store.EXPECT().GetLastBlock().Return(block, nil).AnyTimes()
	store.EXPECT().GetHeightByHash(gomock.Any()).Return(height, nil).AnyTimes()

	store.EXPECT().GetBlockByTx(gomock.Any()).Return(block, nil).AnyTimes()
	store.EXPECT().GetTxHeight(gomock.Any()).Return(height, nil).AnyTimes()
	store.EXPECT().GetBlockByHash(gomock.Any()).Return(block, nil).AnyTimes()

	store.EXPECT().GetTx(txId).Return(&commonPb.Transaction{
		Payload: &commonPb.Payload{
			TxId: txId,
		},
	}, nil).AnyTimes()

	store.EXPECT().GetArchivedPivot().Return(uint64(10)).AnyTimes()
	store.EXPECT().GetArchiveStatus().Return(&storePb.ArchiveStatus{
		Type:                  storePb.StoreType_BFDB,
		MaxAllowArchiveHeight: 10,
		ArchivePivot:          20,
		FileRanges:            make([]*storePb.FileRange, 0),
	}, nil).AnyTimes()

	return store
}

func getTxSimContext(t *testing.T) protocol.TxSimContext {
	ctrl := gomock.NewController(t)
	txSimContext := mock.NewMockTxSimContext(ctrl)

	tx := &commonPb.Transaction{
		Payload: &commonPb.Payload{
			ChainId: chainId,
			TxId:    txId,
		},
		Sender: &commonPb.EndorsementEntry{
			Signer: &accesscontrol.Member{
				OrgId:      "org1",
				MemberInfo: []byte("org1"),
			},
		},
	}

	txSimContext.EXPECT().GetBlockVersion().Return(blockVersion2312).AnyTimes()
	txSimContext.EXPECT().GetTx().Return(tx).AnyTimes()

	txSimContextRes, _ := proto.Marshal(&syscontract.MultiSignInfo{
		Payload: &commonPb.Payload{
			ChainId: chainId,
			TxId:    txId,
		},
	})
	txSimContext.EXPECT().Get(contractName, []byte(txId)).Return(txSimContextRes, nil).AnyTimes()

	block := createNewBlock(chainId, height)
	store := getStoreByBlock(t, block, chainId, txId)

	txSimContext.EXPECT().GetBlockchainStore().Return(store).AnyTimes()

	multiSignInfoBytes := txSimContextRes
	multiSignInfo := &syscontract.MultiSignInfo{}
	err := proto.Unmarshal(multiSignInfoBytes, multiSignInfo)
	require.Nil(t, err)

	contract := &commonPb.Contract{
		Name:        multiSignInfo.ContractName,
		RuntimeType: commonPb.RuntimeType_NATIVE,
		Status:      commonPb.ContractStatus_NORMAL,
		Creator:     nil,
	}

	initParam := make(map[string][]byte)
	for _, parameter := range multiSignInfo.Payload.Parameters {
		if parameter.Key == syscontract.MultiReq_SYS_CONTRACT_NAME.String() ||
			parameter.Key == syscontract.MultiReq_SYS_METHOD.String() {
			continue
		}
		initParam[parameter.Key] = parameter.Value
	}
	byteCode := initParam[syscontract.InitContract_CONTRACT_BYTECODE.String()]
	txSimContext.EXPECT().GetContractByName(syscontract.SystemContract_MULTI_SIGN.String()).Return(&commonPb.Contract{Name: syscontract.SystemContract_MULTI_SIGN.String()}, nil).AnyTimes()
	txSimContext.EXPECT().CallContract(gomock.Any(), contract, multiSignInfo.Method, byteCode, initParam, uint64(0), commonPb.TxType_INVOKE_CONTRACT).Return(&commonPb.ContractResult{}, protocol.ExecOrderTxTypeNormal, commonPb.TxStatusCode_SUCCESS).AnyTimes()

	txSimContext.EXPECT().Put(gomock.Any(), gomock.Any(), gomock.Any()).Return(nil).AnyTimes()

	acProvider := getAccessControlProvider(t)
	txSimContext.EXPECT().GetAccessControl().Return(acProvider, nil).AnyTimes()

	provider := getChainNodesInfoProvider(t)
	txSimContext.EXPECT().GetChainNodesInfoProvider().Return(provider, nil).AnyTimes()

	return txSimContext
}

func getChainNodesInfoProvider(t *testing.T) protocol.ChainNodesInfoProvider {

	ctrl := gomock.NewController(t)

	provider := mock.NewMockChainNodesInfoProvider(ctrl)

	provider.EXPECT().GetChainNodesInfo().Return([]*protocol.ChainNodeInfo{}, nil).AnyTimes()

	return provider
}

func getAccessControlProvider(t *testing.T) protocol.AccessControlProvider {
	ctrl := gomock.NewController(t)

	provider := mock.NewMockAccessControlProvider(ctrl)

	principal := mock.NewMockPrincipal(ctrl)

	provider.EXPECT().CreatePrincipal(gomock.Any(), gomock.Any(), gomock.Any()).Return(principal, nil).AnyTimes()

	member := mock.NewMockMember(ctrl)

	member.EXPECT().GetRole().Return(protocol.RoleAdmin).AnyTimes()
	member.EXPECT().GetUid().Return("org1").AnyTimes()

	provider.EXPECT().NewMember(gomock.Any()).Return(member, nil).AnyTimes()

	return provider
}

func getOrg1Client1Signer() *pbac.Member {
	certStr := "-----BEGIN CERTIFICATE-----\nMIICijCCAi+gAwIBAgIDBS9vMAoGCCqGSM49BAMCMIGKMQswCQYDVQQGEwJDTjEQ\nMA4GA1UECBMHQmVpamluZzEQMA4GA1UEBxMHQmVpamluZzEfMB0GA1UEChMWd3gt\nb3JnMS5jaGFpbm1ha2VyLm9yZzESMBAGA1UECxMJcm9vdC1jZXJ0MSIwIAYDVQQD\nExljYS53eC1vcmcxLmNoYWlubWFrZXIub3JnMB4XDTIwMTIwODA2NTM0M1oXDTI1\nMTIwNzA2NTM0M1owgZExCzAJBgNVBAYTAkNOMRAwDgYDVQQIEwdCZWlqaW5nMRAw\nDgYDVQQHEwdCZWlqaW5nMR8wHQYDVQQKExZ3eC1vcmcxLmNoYWlubWFrZXIub3Jn\nMQ8wDQYDVQQLEwZjbGllbnQxLDAqBgNVBAMTI2NsaWVudDEuc2lnbi53eC1vcmcx\nLmNoYWlubWFrZXIub3JnMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAE56xayRx0\n/a8KEXPxRfiSzYgJ/sE4tVeI/ZbjpiUX9m0TCJX7W/VHdm6WeJLOdCDuLLNvjGTy\nt8LLyqyubJI5AKN7MHkwDgYDVR0PAQH/BAQDAgGmMA8GA1UdJQQIMAYGBFUdJQAw\nKQYDVR0OBCIEIMjAiM2eMzlQ9HzV9ePW69rfUiRZVT2pDBOMqM4WVJSAMCsGA1Ud\nIwQkMCKAIDUkP3EcubfENS6TH3DFczH5dAnC2eD73+wcUF/bEIlnMAoGCCqGSM49\nBAMCA0kAMEYCIQCWUHL0xisjQoW+o6VV12pBXIRJgdeUeAu2EIjptSg2GAIhAIxK\nLXpHIBFxIkmWlxUaanCojPSZhzEbd+8LRrmhEO8n\n-----END CERTIFICATE-----"
	return &pbac.Member{
		OrgId:      "wx-org1.chainmaker.org",
		MemberType: pbac.MemberType_CERT,
		MemberInfo: []byte(certStr),
	}
}

type Mb struct {
}

func (m *Mb) Verify(hashType string, msg []byte, sig []byte) error {
	//TODO implement me
	panic("implement me")
}

func (m *Mb) GetPk() crypto.PublicKey {
	//TODO implement me
	panic("implement me")
}

func (m *Mb) GetMemberId() string {
	return "memberId"
}

func (m *Mb) GetOrgId() string {
	return "orgId"
}

func (m *Mb) GetRole() protocol.Role {
	return "role"
}

func (m *Mb) GetUid() string {
	return "uid"
}

func (m *Mb) GetMember() (*pbac.Member, error) {
	panic("implement me")
}
