package relaycross

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"testing"

	commonPb "chainmaker.org/chainmaker/pb-go/v2/common"

	tcipcommon "chainmaker.org/chainmaker/pb-go/v2/syscontract"

	"chainmaker.org/chainmaker/pb-go/v2/store"

	"chainmaker.org/chainmaker/pb-go/v2/syscontract"

	"chainmaker.org/chainmaker/protocol/v2/test"

	"github.com/stretchr/testify/assert"

	"chainmaker.org/chainmaker/common/v2/crypto"
	"chainmaker.org/chainmaker/pb-go/v2/accesscontrol"
	pbac "chainmaker.org/chainmaker/pb-go/v2/accesscontrol"
	configPb "chainmaker.org/chainmaker/pb-go/v2/config"
	"chainmaker.org/chainmaker/protocol/v2"
	"chainmaker.org/chainmaker/protocol/v2/mock"
	"github.com/gogo/protobuf/proto"
	"github.com/golang/mock/gomock"
)

var (
	crossChainInfos = map[string]*tcipcommon.CrossChainInfo{
		"0": {
			CrossChainId: "0",
			CrossChainMsg: []*tcipcommon.CrossChainMsg{
				{
					ParamData: []int32{1, 2},
				},
			},
			FirstTxContent: &tcipcommon.TxContentWithVerify{
				TxContent: &tcipcommon.TxContent{
					TxId: "0",
				},
			},
		},
	}
)

func initGatewayTest(t *testing.T) *mock.MockTxSimContext {
	ctrl := gomock.NewController(t)
	txSimContext := mock.NewMockTxSimContext(ctrl)
	defer ctrl.Finish()
	acTest := mock.NewMockAccessControlProvider(ctrl)
	acTest.EXPECT().GetHashAlg().Return("SHA256").AnyTimes()
	acTest.EXPECT().NewMember(gomock.Any()).Return(&Mb{}, nil).AnyTimes()
	txSimContext.EXPECT().GetAccessControl().Return(acTest, nil).AnyTimes()

	bcCtrl := gomock.NewController(t)
	bcTest := mock.NewMockBlockchainStore(bcCtrl)
	//cfgTest := &configPb.ChainConfig{Vm: &configPb.Vm{AddrType: configPb.AddrType_CHAINMAKER}}
	cfgTest := &configPb.ChainConfig{Vm: &configPb.Vm{AddrType: configPb.AddrType_ZXL}}
	bcTest.EXPECT().GetLastChainConfig().Return(cfgTest, nil).AnyTimes()
	txSimContext.EXPECT().GetBlockchainStore().Return(bcTest).AnyTimes()
	txSimContext.EXPECT().GetBlockVersion().Return(uint32(2220)).AnyTimes()
	//txSimContext.EXPECT().Get(gomock.Any(), gomock.Any()).Return([]byte{}, nil).AnyTimes()
	txSimContext.EXPECT().Get(contractName, admin).Return([]byte("{\"f6d352783437e112ec12f8738dadd102143f303c\":true}"), nil).AnyTimes()
	txSimContext.EXPECT().Get(gomock.Any(), gomock.Any()).Return([]byte{}, nil).AnyTimes()
	txSimContext.EXPECT().GetSender().Return(&pbac.Member{
		OrgId:      "org1",
		MemberType: accesscontrol.MemberType_PUBLIC_KEY,
		MemberInfo: []byte("-----BEGIN PUBLIC KEY-----\nMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAE5BmG7DVNQcOtDhc/pM+X0SmvBxI2\nfkwmmHe8DfBIcOqjIuqq5KeDEu/xnPUEzBGFS9O3QfKiLkgH0q3QhZXYkw==\n-----END PUBLIC KEY-----"),
	}).AnyTimes()
	txSimContext.EXPECT().Put(gomock.Any(), gomock.Any(), gomock.Any()).AnyTimes()
	txSimContext.EXPECT().GetNoRecord(gomock.Any(), gomock.Any()).DoAndReturn(
		func(name string, key []byte) ([]byte, error) {
			chainConfig := &configPb.ChainConfig{
				Version:  "220",
				AuthType: protocol.Public,
				Vm: &configPb.Vm{
					AddrType: configPb.AddrType_CHAINMAKER,
				},
				Crypto: &configPb.CryptoConfig{
					Hash: "SM3",
				},
			}
			chainConfigByte, _ := proto.Marshal(chainConfig)
			return chainConfigByte, nil
		}).AnyTimes()
	gateway := make(map[string][]byte)
	gateway["key"] = parseGatewayKey(0)
	gateway["value"] = []byte("MTIz")
	list := &ListIter{list: []map[string][]byte{gateway, gateway, gateway}}
	txSimContext.EXPECT().Select(gomock.Any(), gomock.Any(), gomock.Any()).Return(list, nil).AnyTimes()
	tx := &commonPb.Transaction{
		Payload: &commonPb.Payload{
			ChainId: "chain0",
			TxId:    "123456789",
		},
		Sender: &commonPb.EndorsementEntry{
			Signer: &pbac.Member{
				OrgId:      "org1",
				MemberType: accesscontrol.MemberType_PUBLIC_KEY,
				MemberInfo: []byte("-----BEGIN EC PRIVATE KEY-----\nMHcCAQEEIDqK5Jf3ukpwcLGYal2En/C4vdNlzSYGGiIl43S6o/k6oAoGCCqGSM49\nAwEHoUQDQgAE5BmG7DVNQcOtDhc/pM+X0SmvBxI2fkwmmHe8DfBIcOqjIuqq5KeD\nEu/xnPUEzBGFS9O3QfKiLkgH0q3QhZXYkw==\n-----END EC PRIVATE KEY-----"),
			},
		},
	}
	txSimContext.EXPECT().GetTx().Return(tx).AnyTimes()
	return txSimContext
}

func initCrossChainTest(t *testing.T) *mock.MockTxSimContext {
	ctrl := gomock.NewController(t)
	txSimContext := mock.NewMockTxSimContext(ctrl)
	defer ctrl.Finish()
	acTest := mock.NewMockAccessControlProvider(ctrl)
	acTest.EXPECT().GetHashAlg().Return("SHA256").AnyTimes()
	acTest.EXPECT().NewMember(gomock.Any()).Return(&Mb{}, nil).AnyTimes()
	txSimContext.EXPECT().GetAccessControl().Return(acTest, nil).AnyTimes()

	bcCtrl := gomock.NewController(t)
	bcTest := mock.NewMockBlockchainStore(bcCtrl)
	//cfgTest := &configPb.ChainConfig{Vm: &configPb.Vm{AddrType: configPb.AddrType_CHAINMAKER}}
	cfgTest := &configPb.ChainConfig{Vm: &configPb.Vm{AddrType: configPb.AddrType_ZXL}}
	bcTest.EXPECT().GetLastChainConfig().Return(cfgTest, nil).AnyTimes()
	txSimContext.EXPECT().GetBlockchainStore().Return(bcTest).AnyTimes()
	txSimContext.EXPECT().GetBlockVersion().Return(uint32(2220)).AnyTimes()
	txSimContext.EXPECT().Get(contractName, admin).Return([]byte("{\"f6d352783437e112ec12f8738dadd102143f303c\":true}"), nil).AnyTimes()
	txSimContext.EXPECT().GetSender().Return(&pbac.Member{
		OrgId:      "org1",
		MemberType: accesscontrol.MemberType_PUBLIC_KEY,
		MemberInfo: []byte("-----BEGIN PUBLIC KEY-----\nMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAE5BmG7DVNQcOtDhc/pM+X0SmvBxI2\nfkwmmHe8DfBIcOqjIuqq5KeDEu/xnPUEzBGFS9O3QfKiLkgH0q3QhZXYkw==\n-----END PUBLIC KEY-----"),
	}).AnyTimes()
	txSimContext.EXPECT().Put(gomock.Any(), gomock.Any(), gomock.Any()).AnyTimes()
	txSimContext.EXPECT().GetNoRecord(gomock.Any(), gomock.Any()).DoAndReturn(
		func(name string, key []byte) ([]byte, error) {
			chainConfig := &configPb.ChainConfig{
				Version:  "220",
				AuthType: protocol.Public,
				Vm: &configPb.Vm{
					AddrType: configPb.AddrType_CHAINMAKER,
				},
				Crypto: &configPb.CryptoConfig{
					Hash: "SM3",
				},
			}
			chainConfigByte, _ := proto.Marshal(chainConfig)
			return chainConfigByte, nil
		}).AnyTimes()
	gateway := make(map[string][]byte)
	gateway["key"] = parseGatewayKey(0)
	gateway["value"] = []byte("MTIz")
	list := &ListIter{list: []map[string][]byte{gateway, gateway, gateway}}
	txSimContext.EXPECT().Select(gomock.Any(), gomock.Any(), gomock.Any()).Return(list, nil).AnyTimes()
	tx := &commonPb.Transaction{
		Payload: &commonPb.Payload{
			ChainId: "chain0",
			TxId:    "123456789",
		},
		Sender: &commonPb.EndorsementEntry{
			Signer: &pbac.Member{
				OrgId:      "org1",
				MemberType: accesscontrol.MemberType_PUBLIC_KEY,
				MemberInfo: []byte("-----BEGIN EC PRIVATE KEY-----\nMHcCAQEEIDqK5Jf3ukpwcLGYal2En/C4vdNlzSYGGiIl43S6o/k6oAoGCCqGSM49\nAwEHoUQDQgAE5BmG7DVNQcOtDhc/pM+X0SmvBxI2fkwmmHe8DfBIcOqjIuqq5KeD\nEu/xnPUEzBGFS9O3QfKiLkgH0q3QhZXYkw==\n-----END EC PRIVATE KEY-----"),
			},
		},
	}
	txSimContext.EXPECT().GetTx().Return(tx).AnyTimes()
	return txSimContext
}

type ListIter struct {
	list []map[string][]byte
	idx  int
}

func (i *ListIter) Next() bool {
	i.idx++
	return i.idx < len(i.list)+1
}
func (i *ListIter) Value() (*store.KV, error) {
	c := i.list[i.idx-1]
	return &store.KV{
		ContractName: "",
		Key:          c["key"],
		Value:        c["value"],
	}, nil
}
func (i *ListIter) Release() {
	i.list = make([]map[string][]byte, 0)
	i.idx = 0
}

type Mb struct {
}

func (m Mb) GetPk() crypto.PublicKey {
	//TODO implement me
	panic("implement me")
}

func (m Mb) GetMemberId() string {
	return "memberId"
}

func (m Mb) GetOrgId() string {
	return "orgId"
}

func (m Mb) GetRole() protocol.Role {
	return "role"
}

func (m Mb) GetUid() string {
	return "uid"
}

func (m Mb) Verify(hashType string, msg []byte, sig []byte) error {
	panic("implement me")
}

func (m Mb) GetMember() (*pbac.Member, error) {
	panic("implement me")
}

func TestRelayCrossRuntime_Gateway(t *testing.T) {
	ctx := initGatewayTest(t)

	runtime := &RelayCrossRuntime{
		log: &test.GoLogger{},
	}

	params := make(map[string][]byte)
	_, _, err := runtime.SaveGateway(ctx, params)
	assert.NotNil(t, err)

	params[syscontract.SaveGateway_GATEWAY_INFO_BYTE.String()] = []byte("123")
	gatewayId, _, err := runtime.SaveGateway(ctx, params)
	assert.Nil(t, err)
	assert.Equal(t, gatewayId, []byte("0"))

	params[syscontract.UpdateGateway_GATEWAY_INFO_BYTE.String()] = []byte("456")
	_, _, err = runtime.UpdateGateway(ctx, params)
	assert.NotNil(t, err)

	params[syscontract.UpdateGateway_GATEWAY_ID.String()] = gatewayId
	gatewayId, _, err = runtime.UpdateGateway(ctx, params)
	assert.Nil(t, err)
	assert.Equal(t, gatewayId, []byte("0"))

	gatewayNum, err := runtime.GetGatewayNum(ctx, params)
	assert.Nil(t, err)
	assert.Equal(t, gatewayNum, []byte("0"))

	_, err = runtime.GetGateway(ctx, params)
	assert.Nil(t, err)

	_, err = runtime.GetGatewayByRange(ctx, params)
	assert.NotNil(t, err)

	params[syscontract.GetGatewayByRange_START_GATEWAY_ID.String()] = []byte("0")
	params[syscontract.GetGatewayByRange_STOP_GATEWAY_ID.String()] = []byte("1")
	_, err = runtime.GetGatewayByRange(ctx, params)
	assert.Nil(t, err)
}

func TestRelayCrossRuntime_SaveCrossChain(t *testing.T) {
	ctx := initCrossChainTest(t)

	runtime := &RelayCrossRuntime{
		log: &test.GoLogger{},
	}

	ctx.EXPECT().Get(gomock.Any(), gomock.Any()).Return([]byte{}, nil).AnyTimes()
	params := make(map[string][]byte)
	_, e, err := runtime.SaveCrossChainInfo(ctx, params)
	assert.NotNil(t, err)
	assert.Nil(t, e)

	params[syscontract.SaveCrossChainInfo_CROSS_CHAIN_INFO_BYTE.String()], _ = json.Marshal(crossChainInfos["0"])
	crossChainId, e, err := runtime.SaveCrossChainInfo(ctx, params)
	assert.Nil(t, err)
	assert.Equal(t, e[0].EventData[0], string(crossChainId))
}

func TestRelayCrossRuntime_CrossChainTry(t *testing.T) {
	ctx := initCrossChainTest(t)

	runtime := &RelayCrossRuntime{
		log: &test.GoLogger{},
	}

	params := make(map[string][]byte)
	_, e, err := runtime.UpdateCrossChainTry(ctx, params)
	assert.NotNil(t, err)
	assert.Nil(t, e)

	crossChainByte, _ := json.Marshal(crossChainInfos["0"])
	ctx.EXPECT().Get(gomock.Any(), gomock.Any()).Return([]byte(base64.StdEncoding.EncodeToString(crossChainByte)),
		nil).AnyTimes()
	crossChainTryByte, _ := json.Marshal([]*tcipcommon.CrossChainTxUpChain{
		{
			Index: 0,
			TxContentWithVerify: &tcipcommon.TxContentWithVerify{
				TxVerifyResult: tcipcommon.TxVerifyRsult_VERIFY_SUCCESS,
			},
		},
	})

	params[syscontract.UpdateCrossChainTry_CROSS_CHAIN_ID.String()] = []byte("0")
	params[syscontract.UpdateCrossChainTry_CROSS_CHAIN_TX_BYTE.String()] = crossChainTryByte
	crossChainId, e, err := runtime.UpdateCrossChainTry(ctx, params)
	assert.Nil(t, err)
	assert.Equal(t, e[0].EventData[0], string(crossChainId))
}

func TestRelayCrossRuntime_CrossChainResult(t *testing.T) {
	ctx := initCrossChainTest(t)

	runtime := &RelayCrossRuntime{
		log: &test.GoLogger{},
	}

	params := make(map[string][]byte)
	_, e, err := runtime.UpdateCrossChainResult(ctx, params)
	assert.NotNil(t, err)
	assert.Nil(t, e)

	crossChainByte, _ := json.Marshal(crossChainInfos["0"])
	ctx.EXPECT().Get(gomock.Any(), gomock.Any()).Return([]byte(base64.StdEncoding.EncodeToString(crossChainByte)),
		nil).AnyTimes()
	params[syscontract.UpdateCrossChainTry_CROSS_CHAIN_ID.String()] = []byte("0")
	params[syscontract.UpdateCrossChainResult_CROSS_CHAIN_RESULT.String()] = []byte("true")
	crossChainId, e, err := runtime.UpdateCrossChainResult(ctx, params)
	assert.Nil(t, err)
	assert.Equal(t, e[0].EventData[0], string(crossChainId))

	params[syscontract.UpdateCrossChainResult_CROSS_CHAIN_RESULT.String()] = []byte("false")
	crossChainId, e, err = runtime.UpdateCrossChainResult(ctx, params)
	assert.Nil(t, err)
	assert.Equal(t, e[0].EventData[0], string(crossChainId))
}

func TestRelayCrossRuntime_DeleteErrorCrossChainTxList(t *testing.T) {
	ctx := initCrossChainTest(t)

	runtime := &RelayCrossRuntime{
		log: &test.GoLogger{},
	}

	params := make(map[string][]byte)
	_, err := runtime.DeleteErrorCrossChainTxList(ctx, params)
	assert.NotNil(t, err)

	ctx.EXPECT().Del(gomock.Any(), gomock.Any()).Return(nil).AnyTimes()
	params[syscontract.DeleteErrorCrossChainTxList_CROSS_CHAIN_ID.String()] = []byte("0")
	res, err := runtime.DeleteErrorCrossChainTxList(ctx, params)
	assert.Nil(t, err)
	assert.Equal(t, res, []byte("success"))
}

func TestRelayCrossRuntime_UpdateCrossChainConfirm(t *testing.T) {
	ctx := initCrossChainTest(t)

	runtime := &RelayCrossRuntime{
		log: &test.GoLogger{},
	}

	params := make(map[string][]byte)
	_, e, err := runtime.UpdateCrossChainConfirm(ctx, params)
	assert.NotNil(t, err)
	assert.Nil(t, e)

	crossChainConfirmByte, _ := json.Marshal([]*tcipcommon.CrossChainConfirmUpChain{
		{
			Index: 0,
			CrossChainConfirm: &tcipcommon.CrossChainConfirm{
				Message: tcipcommon.Code_GATEWAY_SUCCESS.String(),
			},
		},
	})

	crossChainByte, _ := json.Marshal(crossChainInfos["0"])
	ctx.EXPECT().Get(gomock.Any(), gomock.Any()).Return([]byte(base64.StdEncoding.EncodeToString(crossChainByte)),
		nil).AnyTimes()
	params[syscontract.UpdateCrossChainResult_CROSS_CHAIN_ID.String()] = []byte("0")
	params[syscontract.UpdateCrossChainResult_CROSS_CHAIN_RESULT.String()] = crossChainConfirmByte
	crossChainId, e, err := runtime.UpdateCrossChainConfirm(ctx, params)
	assert.Nil(t, err)
	assert.Equal(t, e[0].EventData[0], string(crossChainId))
}

func TestRelayCrossRuntime_UpdateSrcGatewayConfirm(t *testing.T) {
	ctx := initCrossChainTest(t)

	runtime := &RelayCrossRuntime{
		log: &test.GoLogger{},
	}

	params := make(map[string][]byte)
	_, e, err := runtime.UpdateSrcGatewayConfirm(ctx, params)
	assert.NotNil(t, err)
	assert.Nil(t, e)

	crossChainConfirmByte, _ := proto.Marshal(&tcipcommon.CrossChainConfirm{
		Message: tcipcommon.Code_GATEWAY_SUCCESS.String(),
	})

	crossChainByte, _ := json.Marshal(crossChainInfos["0"])
	ctx.EXPECT().Get(gomock.Any(), gomock.Any()).DoAndReturn(
		func(name string, key []byte) ([]byte, error) {
			if string(key) == string(notEndCrossChainIdListKey) {
				return []byte(fmt.Sprintf("[\"%s\"]", parseCrossChainKey(0))), nil
			}
			return []byte(base64.StdEncoding.EncodeToString(crossChainByte)), nil
		}).AnyTimes()
	params[syscontract.UpdateCrossChainResult_CROSS_CHAIN_ID.String()] = []byte("0")
	params[syscontract.UpdateSrcGatewayConfirm_CONFIRM_RESULT.String()] = crossChainConfirmByte
	crossChainId, e, err := runtime.UpdateSrcGatewayConfirm(ctx, params)
	assert.Nil(t, err)
	assert.Equal(t, e[0].EventData[0], string(crossChainId))
}

func TestRelayCrossRuntime_GetCrossChain(t *testing.T) {
	ctx := initCrossChainTest(t)

	runtime := &RelayCrossRuntime{
		log: &test.GoLogger{},
	}
	ctx.EXPECT().Get(gomock.Any(), gomock.Any()).Return([]byte{}, nil).AnyTimes()

	params := make(map[string][]byte)
	gatewayNum, err := runtime.GetCrossChainNum(ctx, params)
	assert.Nil(t, err)
	assert.Equal(t, gatewayNum, []byte("0"))

	params[syscontract.GetCrossChainInfo_CROSS_CHAIN_ID.String()] = []byte("0")
	_, err = runtime.GetCrossChainInfo(ctx, params)
	assert.Nil(t, err)

	_, err = runtime.GetCrossChainInfoByRange(ctx, params)
	assert.NotNil(t, err)

	params[syscontract.GetCrossChainInfoByRange_START_CROSS_CHAIN_ID.String()] = []byte("0")
	params[syscontract.GetCrossChainInfoByRange_STOP_CROSS_CHAIN_ID.String()] = []byte("1")
	_, err = runtime.GetCrossChainInfoByRange(ctx, params)
	assert.Nil(t, err)
}
