/*
 * Copyright (C) BABEC. All rights reserved.
 * Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package privatecompute

import (
	"crypto/sha256"
	"encoding/binary"
	"encoding/hex"
	"encoding/json"
	"encoding/pem"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"testing"

	"chainmaker.org/chainmaker/common/v2/crypto"
	"chainmaker.org/chainmaker/common/v2/crypto/asym"
	bcx509 "chainmaker.org/chainmaker/common/v2/crypto/x509"
	"chainmaker.org/chainmaker/pb-go/v2/accesscontrol"
	acPb "chainmaker.org/chainmaker/pb-go/v2/accesscontrol"
	commonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	storePb "chainmaker.org/chainmaker/pb-go/v2/store"
	"chainmaker.org/chainmaker/pb-go/v2/syscontract"
	"chainmaker.org/chainmaker/protocol/v2"
	"chainmaker.org/chainmaker/protocol/v2/mock"
	"chainmaker.org/chainmaker/protocol/v2/test"
	"github.com/gogo/protobuf/proto"
	"github.com/golang/mock/gomock"
	"github.com/pingcap/goleveldb/leveldb/util"
	"github.com/stretchr/testify/require"
)

var (
	chainId = "chain1"
	height  = uint64(0)
	txId    = "56789"
	log     = &test.GoLogger{}
	//b       = &BlockRuntime{log: log}
)

var certCaFilename = "testdata/remote_attestation/enclave_cacert.crt"
var certFilename = "testdata/remote_attestation/in_teecert.pem"
var utTestCertname = "testdata/remote_attestation/ut_test.crt"

//var reportFilename = "testdata/remote_attestation/report.dat"

//var (
//	contractName = syscontract.SystemContract_MULTI_SIGN.String()
//)

var (
	contractVersion      = "1.0.0"
	contractName         = "storageContract"
	contractNameParamKey = "contract_name"
	codeHashParamKey     = "code_hash"
	versionParamKey      = "version"
	isDeployParamKey     = "is_deploy"
	reportPramKey        = "report_hash"
	headerParamKey       = "code_header"
	resultParamKey       = "result"

	codeHead     = "608060405234801561001057600080FD5B5061012F806100206000396000F3FE"
	certCaPath   = "testdata/remote_attestation/enclave_cacert.crt"
	certPath     = "testdata/remote_attestation/in_teecert.pem"
	reportPath   = "testdata/remote_attestation/report.dat"
	proofPath    = "testdata/remote_attestation/proof.hex"
	codeBodyPath = "testdata/storage_body.bin"
	OrgId1       = "wx-org1.chainmaker.org"
	usrCrtPath   = "testdata/client1.sign.crt"
	usrSKPath    = "testdata/client1.sign.key"
)

var strPubkey = "-----BEGIN PUBLIC KEY-----\n" +
	"MIIBojANBgkqhkiG9w0BAQEFAAOCAY8AMIIBigKCAYEAqM6qzNzBprMIqUbdDzkU\n" +
	"Fwz8rhi4/ASZB4UNMgxZlbeIZKDqOmQ8I6QjKQ5ZGGMzMeB6oEVv2TQ/8Az8F/mj\n" +
	"ok/w6vFrKM1m6j44W2x4DsO05jqgepOa9jr4Y4YSujOkMedS/mG3jSGLZtl+8nYB\n" +
	"UmquIoacLHNsFmmqB+CY2u1lQ0EaB3XQ/scoQzvtwN54OLUg5xOTViODb59a/w+P\n" +
	"ehu4YdTo5dLUY6idL24hCCVHIZQZMmBzg+lupxC4u8K5gPukEwcsYQ3IVhUHhvWC\n" +
	"AqtRxMywSo3aXfeo+7HJiWouK8dIsL+3VrTQdV8fn9/TImCFDORtyfCGlxLvS9J9\n" +
	"aUcpZrrk+qZUykZY3xRJ8pbhrgGghbmFJw/qCpeyTMfCooIdMezlBFoj2GdVLRLc\n" +
	"LEIief1a4Qg9sg9bJ6Dtj+tEMwjF5opLWm7x36zoakMAu9dQ/O6X0za+jPjY7hTO\n" +
	"BoACd/z8bWJEzVxb6jrFi+cGJY9i/n9CR4lWimzgYPQtAgMBAAE=\n" +
	"-----END PUBLIC KEY-----"

var certData = []byte("-----BEGIN CERTIFICATE-----\nMIIChzCCAi2gAwIBAgIDAwGbMAoGCCqGSM49BAMCMIGKMQswCQYDVQQGEwJDTjEQ\nMA4GA1UECBMHQmVpamluZzEQMA4GA1UEBxMHQmVpamluZzEfMB0GA1UEChMWd3gt\nb3JnMS5jaGFpbm1ha2VyLm9yZzESMBAGA1UECxMJcm9vdC1jZXJ0MSIwIAYDVQQD\nExljYS53eC1vcmcxLmNoYWlubWFrZXIub3JnMB4XDTIwMTIwODA2NTM0M1oXDTI1\nMTIwNzA2NTM0M1owgY8xCzAJBgNVBAYTAkNOMRAwDgYDVQQIEwdCZWlqaW5nMRAw\nDgYDVQQHEwdCZWlqaW5nMR8wHQYDVQQKExZ3eC1vcmcxLmNoYWlubWFrZXIub3Jn\nMQ4wDAYDVQQLEwVhZG1pbjErMCkGA1UEAxMiYWRtaW4xLnNpZ24ud3gtb3JnMS5j\naGFpbm1ha2VyLm9yZzBZMBMGByqGSM49AgEGCCqGSM49AwEHA0IABORqoYNAw8ax\n9QOD94VaXq1dCHguarSKqAruEI39dRkm8Vu2gSHkeWlxzvSsVVqoN6ATObi2ZohY\nKYab2s+/QA2jezB5MA4GA1UdDwEB/wQEAwIBpjAPBgNVHSUECDAGBgRVHSUAMCkG\nA1UdDgQiBCDZOtAtHzfoZd/OQ2Jx5mIMgkqkMkH4SDvAt03yOrRnBzArBgNVHSME\nJDAigCA1JD9xHLm3xDUukx9wxXMx+XQJwtng+9/sHFBf2xCJZzAKBggqhkjOPQQD\nAgNIADBFAiEAiGjIB8Wb8mhI+ma4F3kCW/5QM6tlxiKIB5zTcO5E890CIBxWDICm\nAod1WZHJajgnDQ2zEcFF94aejR9dmGBB/P//\n-----END CERTIFICATE-----")

func readFile(filename string, t *testing.T) []byte {
	file, err := os.Open(filename)
	if err != nil {
		t.Fatalf("open file '%s' error: %v", filename, err)
	}

	data, err := ioutil.ReadAll(file)
	if err != nil {
		t.Fatalf("read file '%v' error: %v", filename, err)
	}
	return data
}

func readHexFile(filename string, t *testing.T) []byte {
	data := readFile(filename, t)
	bytes, _ := hex.DecodeString(string(data))
	return bytes
}

func readFileToHex(filename string, t *testing.T) []byte {
	data := readFile(filename, t)
	return []byte(hex.EncodeToString(data))
}

// TestInTeeCertPemFile tests in tee cert pem file
func TestInTeeCertPemFile(t *testing.T) {
	file, err := os.Open(certPath)
	if err != nil {
		t.Fatalf("open file error: %v", err)
	}

	crtPEM, err := ioutil.ReadAll(file)
	if err != nil {
		t.Fatalf("read file error: %v", err)
	}

	signingPubkey, cryptoPubkey, err := getPubkeyPairFromCert(crtPEM)
	if err != nil {
		t.Fatalf("get pubkey pair error: %v", err)
	}
	fmt.Printf("signing pub key ==> %v \n", signingPubkey.ToStandardKey())
	fmt.Printf("crypto pub key ==> %v \n", cryptoPubkey.ToStandardKey())
}

// TestGetContract test get contract
func TestGetContract(t *testing.T) {
	ds := map[string][]byte{}
	comName := syscontract.SystemContract_PRIVATE_COMPUTE.String() + contractName
	versionKey := comName + "::" + ContractVersion
	ds[versionKey] = []byte(contractVersion)

	headerCode, _ := hex.DecodeString(codeHead)
	bodyCode := readHexFile(codeBodyPath, t)
	headerLen := len(headerCode)
	fullCodes := make([]byte, headerLen+len(bodyCode))
	copy(fullCodes, headerCode)
	copy(fullCodes[headerLen:], bodyCode)

	codeHash := sha256.Sum256(fullCodes)
	ds[comName+"::"+ContractByteHeader+contractVersion] = headerCode
	ds[comName+"::"+ContractByteCode+contractVersion] = bodyCode

	mockCtx := newTxContextMock(ds)
	privateComputeRuntime := PrivateComputeRuntime{
		log: &test.GoLogger{},
	}

	params := map[string][]byte{}
	params[contractNameParamKey] = []byte(contractName)
	params[codeHashParamKey] = codeHash[:]

	_, _ = privateComputeRuntime.GetContract(mockCtx, params)
}

// TestGetData test get data
func TestGetData(t *testing.T) {
	ds := map[string][]byte{}
	comName := syscontract.SystemContract_PRIVATE_COMPUTE.String() + contractName
	valueKey := comName + "::valueKey"
	ds[valueKey] = []byte("value")

	mockCtx := newTxContextMock(ds)
	privateComputeRuntime := PrivateComputeRuntime{
		log: &test.GoLogger{},
	}

	key := "key"
	params := map[string][]byte{}
	params[contractNameParamKey] = []byte(contractName)
	params[key] = []byte("valueKey")
	_, _ = privateComputeRuntime.GetData(mockCtx, params)
}

// TestSaveEnclaveCaCert test save enclave CA cert
func TestSaveEnclaveCaCert(t *testing.T) {
	ds := map[string][]byte{}
	mockCtx := newTxContextMock(ds)

	privateComputeRuntime := PrivateComputeRuntime{
		log: &test.GoLogger{},
	}

	// 读取CA证书
	certCaPem := readFileToHex(certCaPath, t)

	params := map[string][]byte{}
	params["ca_cert"] = certCaPem
	_, _ = privateComputeRuntime.SaveEnclaveCACert(mockCtx, params)
}

func TestSaveDir(t *testing.T) {
	ds := map[string][]byte{}
	mockCtx := newTxContextMock(ds)

	privateComputeRuntime := PrivateComputeRuntime{
		log: &test.GoLogger{},
	}

	params := map[string][]byte{}
	params["order_id"] = []byte("id")
	params["private_dir"] = []byte("dir")
	_, _ = privateComputeRuntime.SaveDir(mockCtx, params)
}

func TestSaveDataDeploy(t *testing.T) {
	ds := map[string][]byte{}

	headerCode, _ := hex.DecodeString(codeHead)
	bodyCode := readHexFile(codeBodyPath, t)
	headerLen := len(headerCode)
	fullCodes := make([]byte, headerLen+len(bodyCode))
	copy(fullCodes, headerCode)
	copy(fullCodes[headerLen:], bodyCode)

	codeHash := sha256.Sum256(fullCodes)
	mockCtx := newTxContextMock(ds)
	r := PrivateComputeRuntime{
		log: &test.GoLogger{},
	}

	params := map[string][]byte{}
	params[contractNameParamKey] = []byte(contractName)
	params[versionParamKey] = []byte(contractVersion)
	params[codeHashParamKey] = codeHash[:]
	params[isDeployParamKey] = []byte(strconv.FormatBool(true))
	params[reportPramKey] = []byte("report hash mock")

	params[headerParamKey] = headerCode

	var computeResult commonPb.ContractResult
	computeResult.Code = 0
	computeResult.GasUsed = 657
	computeResult.Result = bodyCode
	params[resultParamKey], _ = computeResult.Marshal()

	crtPem := readFile(usrCrtPath, t)
	crtBlock, _ := pem.Decode(crtPem)
	crt, _ := bcx509.ParseCertificate(crtBlock.Bytes)
	hashAlgo, _ := bcx509.GetHashFromSignatureAlgorithm(crt.SignatureAlgorithm)

	payloadSt := &syscontract.PrivateDeployPayload{
		CodeBytes:       string(fullCodes),
		ContractName:    contractName,
		ContractVersion: contractVersion,
		CodeHash:        string(codeHash[:]),
		OrgId:           append([]string{}, OrgId1),
	}
	payloadBt, _ := payloadSt.Marshal()
	keyPem := readFile(usrSKPath, t)
	sk, _ := asym.PrivateKeyFromPEM(keyPem, nil)
	sign, _ := sk.SignWithOpts(payloadBt, &crypto.SignOpts{
		Hash: hashAlgo,
	})

	combinedKey := syscontract.SystemContract_PRIVATE_COMPUTE.String() + "global_enclave_id::verification_pub_key"
	pkPem, _ := crt.PublicKey.String()
	ds[combinedKey] = []byte(pkPem)

	deployParamKey := "deploy_req"
	signInfo := &syscontract.SignInfo{
		ClientSign: hex.EncodeToString(sign),
		Cert:       hex.EncodeToString(crtPem),
	}
	req := &syscontract.PrivateDeployRequest{
		SignPair: append([]*syscontract.SignInfo{}, signInfo),
		Payload:  payloadSt,
	}
	request, _ := req.Marshal()
	params[deployParamKey] = request

	rwSet := commonPb.TxRWSet{
		TxId: "tx id mock",
		TxReads: append([]*commonPb.TxRead{}, &commonPb.TxRead{
			Key:          []byte("keyR1"),
			Value:        []byte("valueR1"),
			ContractName: contractName,
		}),
		TxWrites: append([]*commonPb.TxWrite{}, &commonPb.TxWrite{
			Key:          []byte("keyW1"),
			Value:        []byte("valueW1"),
			ContractName: contractName,
		}),
	}
	rwSetBytes, _ := rwSet.Marshal()
	params["rw_set"] = rwSetBytes

	evmResultBytes, _ := r.compactEvmResult(computeResult, rwSet, contractName, contractVersion, codeHash[:],
		params[reportPramKey], request, headerCode)
	resultSign, _ := sk.SignWithOpts(evmResultBytes, &crypto.SignOpts{
		Hash: hashAlgo,
	})

	params["sign"] = resultSign
	_, _ = r.SaveData(mockCtx, params)
}

func TestSaveDataSaveResult(t *testing.T) {
	ds := map[string][]byte{}
	comName := syscontract.SystemContract_PRIVATE_COMPUTE.String() + contractName
	versionSaveKey := comName + "::" + ContractVersion
	ds[versionSaveKey] = []byte(contractVersion)

	headerCode, _ := hex.DecodeString(codeHead)
	bodyCode := readHexFile(codeBodyPath, t)
	headerLen := len(headerCode)
	fullCodes := make([]byte, headerLen+len(bodyCode))
	copy(fullCodes, headerCode)
	copy(fullCodes[headerLen:], bodyCode)

	codeHash := sha256.Sum256(fullCodes)
	ds[comName+"::"+ContractByteHeader+contractVersion] = headerCode
	ds[comName+"::"+ContractByteCode+contractVersion] = bodyCode

	mockCtx := newTxContextMock(ds)
	r := PrivateComputeRuntime{
		log: &test.GoLogger{},
	}

	params := map[string][]byte{}
	params[contractNameParamKey] = []byte(contractName)
	params[versionParamKey] = []byte(contractVersion)
	params[codeHashParamKey] = codeHash[:]
	params[isDeployParamKey] = []byte(strconv.FormatBool(false))
	params[reportPramKey] = []byte("report hash mock")
	params[headerParamKey] = headerCode

	var computeResult commonPb.ContractResult
	computeResult.Code = 0
	computeResult.GasUsed = 657
	computeResult.Result = bodyCode
	params[resultParamKey], _ = computeResult.Marshal()

	crtPem := readFile(usrCrtPath, t)
	crtBlock, _ := pem.Decode(crtPem)
	crt, _ := bcx509.ParseCertificate(crtBlock.Bytes)
	hashAlgo, _ := bcx509.GetHashFromSignatureAlgorithm(crt.SignatureAlgorithm)

	payloadSt := &syscontract.PrivateComputePayload{
		ContractName: contractName,
		CodeHash:     string(codeHash[:]),
		OrgId:        append([]string{}, OrgId1),
	}
	payloadBt, _ := payloadSt.Marshal()
	keyPem := readFile(usrSKPath, t)
	sk, _ := asym.PrivateKeyFromPEM(keyPem, nil)
	sign, _ := sk.SignWithOpts(payloadBt, &crypto.SignOpts{
		Hash: hashAlgo,
	})

	combinedKey := syscontract.SystemContract_PRIVATE_COMPUTE.String() + "global_enclave_id::verification_pub_key"
	pkPem, _ := crt.PublicKey.String()
	ds[combinedKey] = []byte(pkPem)

	privateParamKey := "private_req"
	signInfo := &syscontract.SignInfo{
		ClientSign: hex.EncodeToString(sign),
		Cert:       hex.EncodeToString(crtPem),
	}

	req := &syscontract.PrivateComputeRequest{
		SignPair: append([]*syscontract.SignInfo{}, signInfo),
		Payload:  payloadSt,
	}
	request, _ := req.Marshal()
	params[privateParamKey] = request

	rwSet := commonPb.TxRWSet{
		TxId: "tx id mock",
		TxReads: append([]*commonPb.TxRead{}, &commonPb.TxRead{
			Key:          []byte("keyR1"),
			Value:        []byte("valueR1"),
			ContractName: contractName,
		}),
		TxWrites: append([]*commonPb.TxWrite{}, &commonPb.TxWrite{
			Key:          []byte("keyW1"),
			Value:        []byte("valueW1"),
			ContractName: contractName,
		}),
	}
	rwSetBytes, _ := rwSet.Marshal()
	params["rw_set"] = rwSetBytes

	evmResultBytes, _ := r.compactEvmResult(computeResult, rwSet, contractName, contractVersion, codeHash[:],
		params[reportPramKey], request, headerCode)
	resultSign, _ := sk.SignWithOpts(evmResultBytes, &crypto.SignOpts{
		Hash: hashAlgo,
	})

	params["sign"] = resultSign
	_, _ = r.SaveData(mockCtx, params)
}

func TestSaveEnclaveReport(t *testing.T) {
	ds := map[string][]byte{}
	mockCtx := newTxContextMock(ds)

	privateComputeRuntime := PrivateComputeRuntime{
		log: &test.GoLogger{},
	}

	// 读取report
	report := readFileToHex(reportPath, t)

	params := map[string][]byte{}
	params["enclave_id"] = []byte("global_enclave_id")
	params["report"] = report
	_, _ = privateComputeRuntime.SaveEnclaveReport(mockCtx, params)
}

func TestGetDir(t *testing.T) {
	ds := map[string][]byte{}
	ds["id"] = []byte("value")
	mockCtx := newTxContextMock(ds)

	privateComputeRuntime := PrivateComputeRuntime{
		log: &test.GoLogger{},
	}

	params := map[string][]byte{}
	params["order_id"] = []byte("id")
	_, _ = privateComputeRuntime.GetDir(mockCtx, params)
}

func TestGetEnclaveCACert(t *testing.T) {
	certCaPem := readFileToHex(certCaPath, t)
	ds := map[string][]byte{}
	ds[syscontract.SystemContract_PRIVATE_COMPUTE.String()+"ca_cert"] = certCaPem
	mockCtx := newTxContextMock(ds)

	privateComputeRuntime := PrivateComputeRuntime{
		log: &test.GoLogger{},
	}

	params := map[string][]byte{}
	_, _ = privateComputeRuntime.GetEnclaveCACert(mockCtx, params)
}

func TestGetEnclaveProof(t *testing.T) {
	ds := map[string][]byte{}
	combinedKey := syscontract.SystemContract_PRIVATE_COMPUTE.String() + "global_enclave_id::proof"

	proof := readFile(proofPath, t)
	ds[combinedKey] = proof
	mockCtx := newTxContextMock(ds)

	privateComputeRuntime := PrivateComputeRuntime{
		log: &test.GoLogger{},
	}

	params := map[string][]byte{}
	params["enclave_id"] = []byte("global_enclave_id")
	_, _ = privateComputeRuntime.GetEnclaveProof(mockCtx, params)
}

func TestGetCheckCallerCertAuth(t *testing.T) {
	ds := map[string][]byte{}
	mockCtx := newTxContextMock(ds)
	r := PrivateComputeRuntime{
		log: &test.GoLogger{},
	}

	crtPem := readFile(usrCrtPath, t)
	crtBlock, _ := pem.Decode(crtPem)
	crt, _ := bcx509.ParseCertificate(crtBlock.Bytes)
	hashAlgo, _ := bcx509.GetHashFromSignatureAlgorithm(crt.SignatureAlgorithm)

	keyPem := readFile(usrSKPath, t)
	sk, _ := asym.PrivateKeyFromPEM(keyPem, nil)
	sign, _ := sk.SignWithOpts([]byte(codeHead), &crypto.SignOpts{
		Hash: hashAlgo,
	})

	signInfo := &syscontract.SignInfo{
		ClientSign: hex.EncodeToString(sign),
		Cert:       hex.EncodeToString(crtPem),
	}

	var signPairs []*syscontract.SignInfo
	signPairs = append(signPairs, signInfo)

	params := map[string][]byte{}
	params["sign_pairs"], _ = json.Marshal(signPairs)

	params["payload"] = []byte(codeHead)

	orgIds, _ := json.Marshal(OrgId1)
	params["org_ids"] = orgIds

	_, _ = r.CheckCallerCertAuth(mockCtx, params)
}

func TestGetEnclaveEncryptPubKey(t *testing.T) {
	keyPem := readFile(usrSKPath, t)
	sk, _ := asym.PrivateKeyFromPEM(keyPem, nil)

	ds := map[string][]byte{}
	comKey := syscontract.SystemContract_PRIVATE_COMPUTE.String() + "enclave_id_1::encrypt_pub_key"
	ds[comKey], _ = sk.PublicKey().Bytes()

	mockCtx := newTxContextMock(ds)
	privateComputeRuntime := PrivateComputeRuntime{
		log: &test.GoLogger{},
	}

	params := map[string][]byte{}
	params["enclave_id"] = []byte("enclave_id_1")
	_, _ = privateComputeRuntime.GetEnclaveEncryptPubKey(mockCtx, params)
}

func TestGetEnclaveVerificationPubKey(t *testing.T) {
	keyPem := readFile(usrSKPath, t)
	sk, _ := asym.PrivateKeyFromPEM(keyPem, nil)

	ds := map[string][]byte{}
	comKey := syscontract.SystemContract_PRIVATE_COMPUTE.String() + "enclave_id_1::verification_pub_key"
	ds[comKey], _ = sk.PublicKey().Bytes()

	mockCtx := newTxContextMock(ds)
	privateComputeRuntime := PrivateComputeRuntime{
		log: &test.GoLogger{},
	}

	params := map[string][]byte{}
	params["enclave_id"] = []byte("enclave_id_1")
	_, _ = privateComputeRuntime.GetEnclaveVerificationPubKey(mockCtx, params)
}

func TestGetEnclaveReport(t *testing.T) {
	ds := map[string][]byte{}
	comKey := syscontract.SystemContract_PRIVATE_COMPUTE.String() + "enclave_id_1::report"
	report := readFile(reportPath, t)
	ds[comKey] = report

	mockCtx := newTxContextMock(ds)
	privateComputeRuntime := PrivateComputeRuntime{
		log: &test.GoLogger{},
	}

	params := map[string][]byte{}
	params["enclave_id"] = []byte("enclave_id_1")
	_, _ = privateComputeRuntime.GetEnclaveReport(mockCtx, params)
}

func TestGetEnclaveChallenge(t *testing.T) {
	ds := map[string][]byte{}
	comKey := syscontract.SystemContract_PRIVATE_COMPUTE.String() + "enclave_id_1::challenge"
	ds[comKey] = []byte("challenge mock")

	mockCtx := newTxContextMock(ds)
	privateComputeRuntime := PrivateComputeRuntime{
		log: &test.GoLogger{},
	}

	params := map[string][]byte{}
	params["enclave_id"] = []byte("enclave_id_1")
	_, _ = privateComputeRuntime.GetEnclaveChallenge(mockCtx, params)
}

func TestGetEnclaveSignature(t *testing.T) {
	ds := map[string][]byte{}
	comKey := syscontract.SystemContract_PRIVATE_COMPUTE.String() + "enclave_id_1::signature"
	ds[comKey] = []byte("signature mock")

	mockCtx := newTxContextMock(ds)
	privateComputeRuntime := PrivateComputeRuntime{
		log: &test.GoLogger{},
	}

	params := map[string][]byte{}
	params["enclave_id"] = []byte("enclave_id_1")
	_, _ = privateComputeRuntime.GetEnclaveSignature(mockCtx, params)
}

func TestSaveRemoteAttestation(t *testing.T) {
	privateComputeRuntime := PrivateComputeRuntime{
		log: &test.GoLogger{},
	}

	caCertPem := readFile(certCaPath, t)
	report := readFileToHex(reportPath, t)

	ds := map[string][]byte{}
	mockCtx := newTxContextMock(ds)
	//reportKey := syscontract.SystemContract_PRIVATE_COMPUTE.String() + "global_enclave_id::report"
	//ds[reportKey] = report
	//caCertKey := syscontract.SystemContract_PRIVATE_COMPUTE.String() + "::ca_cert"
	//ds[caCertKey] = caCertPem

	params := map[string][]byte{}
	params["report"] = report
	params["enclave_id"] = []byte("global_enclave_id")
	_, err := privateComputeRuntime.SaveEnclaveReport(mockCtx, params)
	if err != nil {
		t.Fatalf("Save enclave report error: %v", err)
	}
	params["ca_cert"] = caCertPem
	_, err = privateComputeRuntime.SaveEnclaveCACert(mockCtx, params)
	if err != nil {
		t.Fatalf("Save enclave ca cert error: %v", err)
	}

	proofFile, err := os.Open(proofPath)
	if err != nil {
		t.Fatalf("open file '%s' error: %v", proofPath, err)
	}

	proofHex, err := ioutil.ReadAll(proofFile)
	if err != nil {
		t.Fatalf("read file '%v' error: %v", proofFile, err)
	}

	params["proof"] = proofHex

	_, err = privateComputeRuntime.SaveRemoteAttestation(mockCtx, params)

	if err != nil {
		require.NotNil(t, err)
	}
	//for key, val := range ds {
	//	fmt.Printf("key = %v, val = %x \n", key, val)q
	//}
}

func TestNewPrivateComputeContact(t *testing.T) {
	NewPrivateComputeContact(log)
}

func TestGetValue(t *testing.T) {

	p := NewPrivateComputeRuntime()

	txSimContext := getTxSimContext(t)

	params := map[string][]byte{}
	params["order_id"] = []byte("test")

	res, err := p.getValue(txSimContext, string(params["order_id"]))

	require.Nil(t, err)
	fmt.Println(string(res))

}

func TestSaveContract(t *testing.T) {

	p := NewPrivateComputeRuntime()

	txSimContext := getTxSimContext(t)

	params := map[string][]byte{}
	params["order_id"] = []byte("test")
	params["contract_name"] = []byte(contractName)
	params["version"] = []byte("1.0.0")
	params["code_hash"] = []byte("test")
	params["code_header"] = []byte("rest")
	params["result"] = []byte("rest")

	name := string(params["contract_name"])
	version := string(params["version"])
	codeHash := string(params["code_hash"])
	codeHeader := string(params["code_header"])
	cRes := params["result"]

	var result commonPb.ContractResult
	err := result.Unmarshal(cRes)
	require.NotNil(t, err)
	err = p.saveContract(txSimContext, name, version, []byte(codeHeader), result.Result, codeHash)

	require.NotNil(t, err)
}

func TestSaveData(t *testing.T) {

	p := NewPrivateComputeRuntime()

	txSimContext := getTxSimContext(t)

	params := map[string][]byte{}
	params["order_id"] = []byte("test")
	params["contract_name"] = []byte(contractName)
	params["version"] = []byte("1.0.0")
	params["code_hash"] = []byte("test")
	params["code_header"] = []byte("rest")
	params["result"] = []byte("rest")
	params["is_deploy"] = []byte("true")
	params["deploy_req"] = []byte("test")

	result, err := proto.Marshal(&commonPb.ContractResult{
		Result: []byte("test"),
	})
	require.Nil(t, err)
	params["result"] = result

	res, err := p.SaveData(txSimContext, params)

	require.NotNil(t, err)
	fmt.Println(string(res))
}

func TestGetDeployRequest(t *testing.T) {

	p := NewPrivateComputeRuntime()
	var err error

	params := map[string][]byte{}
	params["order_id"] = []byte("test")
	params["contract_name"] = []byte(contractName)
	params["version"] = []byte("1.0.0")
	params["code_hash"] = []byte("test")
	params["code_header"] = []byte("rest")
	params["result"] = []byte("rest")
	params["is_deploy"] = []byte("true")

	params["result"], err = proto.Marshal(&commonPb.ContractResult{
		Result: []byte("test"),
	})
	require.Nil(t, err)

	cert := readFile(certFilename, t)
	clientSign := readFile(certCaFilename, t)

	req := &syscontract.PrivateDeployRequest{
		SignPair: []*syscontract.SignInfo{
			{
				Cert:       string(cert),
				ClientSign: string(clientSign),
			},
		},
	}

	params["deploy_req"], err = proto.Marshal(req)
	require.Nil(t, err)

	req, err = p.getDeployRequest(params)

	require.Nil(t, err)

	fmt.Println(req)
}

func TestParseParamsForAuthChecking(t *testing.T) {

	p := NewPrivateComputeRuntime()

	var err error

	params := map[string][]byte{}
	params["order_id"] = []byte("test")
	params["contract_name"] = []byte(contractName)
	params["version"] = []byte("1.0.0")
	params["code_hash"] = []byte("test")
	params["code_header"] = []byte("rest")
	params["result"] = []byte("rest")
	params["is_deploy"] = []byte("true")
	params["sign"] = []byte("test")

	result := &commonPb.ContractResult{
		Result: []byte("test"),
	}

	params["result"], err = proto.Marshal(result)
	require.Nil(t, err)

	cert := readFile(certFilename, t)
	clientSign := readFile(certCaFilename, t)

	req := &syscontract.PrivateDeployRequest{
		SignPair: []*syscontract.SignInfo{
			{
				Cert:       string(cert),
				ClientSign: string(clientSign),
			},
		},
		Payload: &syscontract.PrivateDeployPayload{
			OrgId: []string{"Org1"},
		},
	}

	params["deploy_req"], err = proto.Marshal(req)
	require.Nil(t, err)

	_, payloadBytes, signPairs, orgIds, err := p.parseParamsForAuthChecking(true, params)

	require.Nil(t, err)
	fmt.Println(string(payloadBytes))
	fmt.Println(signPairs)
	fmt.Println(orgIds)
}

// TODO
/*func TestVerifySign(t *testing.T) {
	p := NewPrivateComputeRuntime()

	var err error

	params := map[string][]byte{}
	params["order_id"] = []byte("test")
	params["contract_name"] = []byte(contractName)
	params["version"] = []byte("1.0.0")
	params["code_hash"] = []byte("test")
	params["code_header"] = []byte("rest")
	params["result"] = []byte("rest")
	params["is_deploy"] = []byte("true")
	params["sign"] = []byte("test")
	params["report_hash"] = []byte("test")

	name := string(params["contract_name"])
	version := string(params["version"])
	codeHash := string(params["code_hash"])
	reportHash := string(params["report_hash"])
	sign := params["sign"]

	txSimContext := getTxSimContext(t)

	txRwSet := txSimContext.GetTxRWSet(false)

	result := &commonPb.ContractResult{
		Result: []byte("test"),
	}

	params["result"], err = proto.Marshal(result)
	require.Nil(t, err)

	cert := readFile(certFilename, t)
	clientSign := readFile(certCaFilename, t)

	req := &syscontract.PrivateDeployRequest{
		SignPair: []*syscontract.SignInfo{
			{
				Cert:       string(cert),
				ClientSign: string(clientSign),
			},
		},
		Payload: &syscontract.PrivateDeployPayload{
			OrgId: []string{"Org1"},
		},
	}

	params["deploy_req"], err = proto.Marshal(req)
	require.Nil(t, err)

	requestBytes, payloadBytes, signPairs, orgIds, err := p.parseParamsForAuthChecking(true, params)

	require.Nil(t, err)
	fmt.Println(string(payloadBytes))
	fmt.Println(signPairs)
	fmt.Println(orgIds)

	err = p.verifySign(txSimContext, *result, *txRwSet, name, version, codeHash, reportHash, requestBytes, params["code_header"], sign)
	require.NotNil(t, err)
}*/

func TestCheckCodeBytesHash(t *testing.T) {

	p := NewPrivateComputeRuntime()

	var err error

	i := util.Hash([]byte("rest"), uint32(123456))
	codeHashByte := make([]byte, 8)
	binary.BigEndian.PutUint32(codeHashByte, i)

	txSimContext := getTxSimContext(t)

	params := map[string][]byte{}
	params["order_id"] = []byte("test")
	params["contract_name"] = []byte(contractName)
	params["version"] = []byte("1.0.0")
	params["code_hash"] = codeHashByte
	params["code_header"] = []byte("test")
	params["result"] = []byte("rest")
	params["is_deploy"] = []byte("true")
	params["sign"] = []byte("test")
	name := string(params["contract_name"])
	version := string(params["version"])
	codeHash := string(params["code_hash"])

	combinationName := syscontract.SystemContract_PRIVATE_COMPUTE.String() + name

	result := &commonPb.ContractResult{
		Result: []byte("test"),
	}

	params["result"], err = proto.Marshal(result)
	require.Nil(t, err)

	cert := readFile(certFilename, t)
	clientSign := readFile(certCaFilename, t)

	req := &syscontract.PrivateDeployRequest{
		SignPair: []*syscontract.SignInfo{
			{
				Cert:       string(cert),
				ClientSign: string(clientSign),
			},
		},
		Payload: &syscontract.PrivateDeployPayload{
			OrgId: []string{"Org1"},
		},
	}

	params["deploy_req"], err = proto.Marshal(req)
	require.Nil(t, err)

	err = p.checkCodeBytesHash(txSimContext, combinationName, name, version, codeHash)
	require.NotNil(t, err)
}

func NewPrivateComputeRuntime() *PrivateComputeRuntime {
	return &PrivateComputeRuntime{
		log: log,
	}
}

func createNewBlock(chainId string, height uint64) *commonPb.Block {

	block := &commonPb.Block{
		Header: &commonPb.BlockHeader{
			BlockHeight:    height,
			PreBlockHash:   nil,
			BlockHash:      nil,
			BlockVersion:   0,
			DagHash:        nil,
			RwSetRoot:      nil,
			BlockTimestamp: 1,
			Proposer:       &acPb.Member{MemberInfo: []byte{1, 2, 3}},
			ConsensusArgs:  nil,
			TxCount:        0,
			Signature:      nil,
			ChainId:        chainId,
		},
		Dag: &commonPb.DAG{
			Vertexes: nil,
		},
	}
	block.Header.PreBlockHash = nil

	return block
}

func getStoreByBlock(t *testing.T, block *commonPb.Block, chainId string, txId string) protocol.BlockchainStore {

	ctrl := gomock.NewController(t)
	store := mock.NewMockBlockchainStore(ctrl)

	store.EXPECT().GetBlock(gomock.Any()).Return(block, nil).AnyTimes()
	store.EXPECT().GetBlockWithRWSets(gomock.Any()).Return(nil, nil).AnyTimes()
	store.EXPECT().GetBlockHeaderByHeight(gomock.Any()).Return(&commonPb.BlockHeader{
		ChainId: chainId,
	}, nil).AnyTimes()

	store.EXPECT().GetLastConfigBlock().Return(block, nil).AnyTimes()
	store.EXPECT().GetLastBlock().Return(block, nil).AnyTimes()
	store.EXPECT().GetHeightByHash(gomock.Any()).Return(height, nil).AnyTimes()

	store.EXPECT().GetBlockByTx(gomock.Any()).Return(block, nil).AnyTimes()
	store.EXPECT().GetTxHeight(gomock.Any()).Return(height, nil).AnyTimes()
	store.EXPECT().GetBlockByHash(gomock.Any()).Return(block, nil).AnyTimes()

	store.EXPECT().GetTx(txId).Return(&commonPb.Transaction{
		Payload: &commonPb.Payload{
			TxId: txId,
		},
	}, nil).AnyTimes()

	store.EXPECT().GetArchivedPivot().Return(uint64(10)).AnyTimes()
	store.EXPECT().GetArchiveStatus().Return(&storePb.ArchiveStatus{
		Type:                  storePb.StoreType_BFDB,
		MaxAllowArchiveHeight: 10,
		ArchivePivot:          20,
		FileRanges:            make([]*storePb.FileRange, 0),
	}, nil).AnyTimes()

	return store
}

func getTxSimContext(t *testing.T) protocol.TxSimContext {
	ctrl := gomock.NewController(t)
	txSimContext := mock.NewMockTxSimContext(ctrl)

	tx := &commonPb.Transaction{
		Payload: &commonPb.Payload{
			ChainId: chainId,
			TxId:    txId,
		},
		Sender: &commonPb.EndorsementEntry{
			Signer: &accesscontrol.Member{
				OrgId:      "org1",
				MemberInfo: []byte("org1"),
			},
		},
	}

	txSimContext.EXPECT().GetBlockVersion().Return(uint32(2030300)).AnyTimes()
	txSimContext.EXPECT().GetTx().Return(tx).AnyTimes()

	txSimContextRes, _ := proto.Marshal(&syscontract.MultiSignInfo{
		Payload: &commonPb.Payload{
			ChainId: chainId,
			TxId:    txId,
		},
	})

	//cert := readFile(certFilename, t)
	cert := readFile(utTestCertname, t)
	//txSimContext.EXPECT().Get(syscontract.SystemContract_PRIVATE_COMPUTE.String()+"global_enclave_id", []byte("verification_pub_key")).Return(cert, nil).AnyTimes()
	txSimContext.EXPECT().Get(gomock.Any(), gomock.Any()).Return(cert, nil).AnyTimes()
	txSimContext.EXPECT().GetTxRWSet(false).Return(&commonPb.TxRWSet{
		TxId: txId,
	}).AnyTimes()

	block := createNewBlock(chainId, height)
	store := getStoreByBlock(t, block, chainId, txId)

	txSimContext.EXPECT().GetBlockchainStore().Return(store).AnyTimes()

	multiSignInfoBytes := txSimContextRes
	multiSignInfo := &syscontract.MultiSignInfo{}
	err := proto.Unmarshal(multiSignInfoBytes, multiSignInfo)
	require.Nil(t, err)

	contract := &commonPb.Contract{
		Name:        multiSignInfo.ContractName,
		RuntimeType: commonPb.RuntimeType_NATIVE,
		Status:      commonPb.ContractStatus_NORMAL,
		Creator:     nil,
	}

	initParam := make(map[string][]byte)
	for _, parameter := range multiSignInfo.Payload.Parameters {
		if parameter.Key == syscontract.MultiReq_SYS_CONTRACT_NAME.String() ||
			parameter.Key == syscontract.MultiReq_SYS_METHOD.String() {
			continue
		}
		initParam[parameter.Key] = parameter.Value
	}
	byteCode := initParam[syscontract.InitContract_CONTRACT_BYTECODE.String()]
	txSimContext.EXPECT().CallContract(contract, contract, multiSignInfo.Method, byteCode, initParam, uint64(0), commonPb.TxType_INVOKE_CONTRACT).Return(&commonPb.ContractResult{}, protocol.ExecOrderTxTypeNormal, commonPb.TxStatusCode_SUCCESS).AnyTimes()

	txSimContext.EXPECT().Put(gomock.Any(), gomock.Any(), gomock.Any()).Return(nil).AnyTimes()

	acProvider := getAccessControlProvider(t)
	txSimContext.EXPECT().GetAccessControl().Return(acProvider, nil).AnyTimes()

	provider := getChainNodesInfoProvider(t)
	txSimContext.EXPECT().GetChainNodesInfoProvider().Return(provider, nil).AnyTimes()

	return txSimContext
}

func getChainNodesInfoProvider(t *testing.T) protocol.ChainNodesInfoProvider {

	ctrl := gomock.NewController(t)

	provider := mock.NewMockChainNodesInfoProvider(ctrl)

	provider.EXPECT().GetChainNodesInfo().Return([]*protocol.ChainNodeInfo{}, nil).AnyTimes()

	return provider
}

func getAccessControlProvider(t *testing.T) protocol.AccessControlProvider {
	ctrl := gomock.NewController(t)

	provider := mock.NewMockAccessControlProvider(ctrl)

	principal := mock.NewMockPrincipal(ctrl)
	blockVersion2330 := uint(2030300)

	provider.EXPECT().CreatePrincipal(gomock.Any(), gomock.Any(), gomock.Any()).Return(principal, nil).AnyTimes()

	provider.EXPECT().GetValidEndorsements(principal, blockVersion2330).Return([]*commonPb.EndorsementEntry{
		{
			Signer: &accesscontrol.Member{
				OrgId:      "org1",
				MemberInfo: []byte("org1"),
			},
		}}, nil).AnyTimes()

	provider.EXPECT().VerifyTxPrincipal(principal, nil, blockVersion2330).Return(true, nil).AnyTimes()

	member := mock.NewMockMember(ctrl)

	member.EXPECT().GetRole().Return(protocol.RoleAdmin).AnyTimes()
	member.EXPECT().GetUid().Return("org1").AnyTimes()

	provider.EXPECT().NewMember(gomock.Any()).Return(member, nil).AnyTimes()

	return provider
}

func TestPrivateComputeRuntime_VerifyByEnclaveCert(t *testing.T) {
	type fields struct {
		log protocol.Logger
	}
	type args struct {
		context   protocol.TxSimContext
		enclaveId []byte
		data      []byte
		sign      []byte
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		{
			name: "test0", // cert.PublicKey.Verify err [nil signature],return err
			fields: fields{
				log: newMockLogger(t),
			},
			args: args{
				context: getTxSimContext(t),
			},
			want:    false,
			wantErr: true,
		},
		{
			name: "test1", // get enclave cert, return err
			fields: fields{
				log: newMockLogger(t),
			},
			args: args{
				context: func() protocol.TxSimContext {
					txSimContext := newMockTxSimContext(t)
					txSimContext.EXPECT().Get(gomock.Any(), gomock.Any()).Return(nil, errors.New("test err"))
					return txSimContext
				}(),
			},
			want:    false,
			wantErr: true,
		},
		{
			name: "test2", // parse enclave certificate failed-> decode pem failed, invalid certificate, return err
			fields: fields{
				log: newMockLogger(t),
			},
			args: args{
				context: func() protocol.TxSimContext {
					txSimContext := newMockTxSimContext(t)
					txSimContext.EXPECT().Get(gomock.Any(), gomock.Any()).Return(nil, nil)
					return txSimContext
				}(),
			},
			want:    false,
			wantErr: true,
		},
		{
			name: "test3", // enclave certificate verify failed, return err
			fields: fields{
				log: newMockLogger(t),
			},
			args: args{
				context: func() protocol.TxSimContext {
					txSimContext := newMockTxSimContext(t)
					txSimContext.EXPECT().Get(gomock.Any(), gomock.Any()).Return(certData, nil)
					return txSimContext
				}(),
			},
			want:    false,
			wantErr: true,
		},
		{
			name: "test4", // enclave certificate verify failed, return err
			fields: fields{
				log: newMockLogger(t),
			},
			args: args{
				context: func() protocol.TxSimContext {
					txSimContext := newMockTxSimContext(t)
					txSimContext.EXPECT().Get(gomock.Any(), gomock.Any()).Return(certData, nil)
					return txSimContext
				}(),
				data: certData,
				sign: []byte(strPubkey),
			},
			want:    false,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &PrivateComputeRuntime{
				log: tt.fields.log,
			}
			got, err := r.VerifyByEnclaveCert(tt.args.context, tt.args.enclaveId, tt.args.data, tt.args.sign)
			if (err != nil) != tt.wantErr {
				t.Errorf("VerifyByEnclaveCert() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("VerifyByEnclaveCert() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func newMockLogger(t *testing.T) *mock.MockLogger {
	ctrl := gomock.NewController(t)
	logger := mock.NewMockLogger(ctrl)
	logger.EXPECT().Debugf(gomock.Any(), gomock.Any()).AnyTimes()
	logger.EXPECT().Infof(gomock.Any(), gomock.Any()).AnyTimes()
	logger.EXPECT().Errorf(gomock.Any(), gomock.Any()).AnyTimes()
	logger.EXPECT().Error(gomock.Any()).AnyTimes()
	return logger
}

func newMockTxSimContext(t *testing.T) *mock.MockTxSimContext {
	ctrl := gomock.NewController(t)
	simContext := mock.NewMockTxSimContext(ctrl)
	return simContext
}
